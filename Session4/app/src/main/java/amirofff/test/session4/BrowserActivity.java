package amirofff.test.session4;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;

public class BrowserActivity extends AppCompatActivity {

    EditText url;
    WebView myWeb;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browser);

        url=(EditText) findViewById(R.id.url);
        myWeb=(WebView) findViewById(R.id.myWeb);
        myWeb.getSettings().setJavaScriptEnabled(true);

        // agar nemikhahim url dar khode barname baz shavad bayad khatte paiin ra comment konim
//        myWeb.setWebViewClient(new WebViewClient());


        findViewById(R.id.show).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myWeb.loadUrl(url.getText().toString());
            }
        });
    }
}
