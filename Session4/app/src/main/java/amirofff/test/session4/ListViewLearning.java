package amirofff.test.session4;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import amirofff.test.session4.adapters.StudentListAdapter;

public class ListViewLearning extends AppCompatActivity {

    ListView studentList ;
    PublicMethods pm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view_learning);
        pm = new PublicMethods(this);
        studentList = (ListView) findViewById(R.id.studentList);
        String names[]={
                "Ali",
                "Hassan",
                "Hossein",
                "Sajjad",
                "Bagher",
                "Sadegh",
                "Zahra",
                "Fatemeh"
        };


        StudentListAdapter adapter = new StudentListAdapter(this,names);
        studentList.setAdapter(adapter);

        studentList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                String name = (String) adapterView.getItemAtPosition(position);
                pm.showToast(name);
            }
        });

    }
}
