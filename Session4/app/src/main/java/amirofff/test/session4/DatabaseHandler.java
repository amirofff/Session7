package amirofff.test.session4;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;


/**
 * Created by macbook on 6/22/2017 AD.
 */

public class DatabaseHandler extends SQLiteOpenHelper {

    String StudentTBL = "" +
            " CREATE TABLE students (" +
            " _ID INTEGER AUTO INCREMENT PRIMARY KEY ," +
            " name TEXT ," +
            " family TEXT ," +
            " mobile TEXT " +
            " ) " ;


    public DatabaseHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(StudentTBL);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {



    }

    public void inserStudent(String name, String family, String mobile) {


        String insertQuery = "" +
                " INSERT INTO students (name , family , mobile )" +
                " VALUES( '" + name + "' , '" + family + "' , '" + mobile + "')";


        SQLiteDatabase db = this.getWritableDatabase();

        try {
            db.execSQL(insertQuery);
        }catch (Exception e){

        }

        db.close();

    }


    public String getStudens(){
        String result = "";
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT name,family,mobile from students" , null);
        while (cursor.moveToNext()) {

            result += cursor.getString(0)+
                    " " +
                    cursor.getString(1)+
                    " " +
                    cursor.getString(2)+
                    "\n"; }

        db.close();
        return result;
    }

}
