package amirofff.test.session4;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import static amirofff.test.session4.R.id.family;
import static amirofff.test.session4.R.id.mobile;
import static amirofff.test.session4.R.id.name;

public class DatabaseFormActivity extends AppCompatActivity {

    EditText name;
    EditText family;
    EditText mobile;
    Button save;
    TextView showResults;
    DatabaseHandler handler;
    Button show;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_database_form);

        name = (EditText) findViewById(R.id.name);
        family = (EditText) findViewById(R.id.family);
        mobile = (EditText) findViewById(R.id.mobile);
        save = (Button) findViewById(R.id.save);
        showResults =(TextView) findViewById(R.id.showResults);
        show = (Button) findViewById(R.id.show);
        handler = new DatabaseHandler(
                this , "sematec.db" , null , 1

        );


        //insert codes
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handler.inserStudent(name.getText().toString() ,
                        family.getText().toString() ,
                        mobile.getText().toString()

                        );
                name.setText("");
                family.setText("");
                mobile.setText("");
                Toast.makeText(DatabaseFormActivity.this, "data has been saved", Toast.LENGTH_SHORT).show();
            }
        });

        //get results
        show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String result = handler.getStudens();
                showResults.setText(result);
            }
        });

    }
}
