package amirofff.test.session4;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.URL;

import amirofff.test.session4.models.WeatherModel;

public class WeatherActivity extends AppCompatActivity {

    TextView tempInt;
    TextView pressureInt;
    TextView humidityInt;
    TextView windsppedInt;
    TextView city;
//    TextView weatherInt;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        tempInt = (TextView) findViewById(R.id.temp);
        pressureInt = (TextView) findViewById(R.id.pressure);
        humidityInt = (TextView) findViewById(R.id.humidity);
        windsppedInt = (TextView) findViewById(R.id.windspeed);
//        weatherInt = (TextView) findViewById(R.id.weather1);
        city = (TextView) findViewById(R.id.city);

        findViewById(R.id.btnTehran).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getWeatherTehran();
                city.setText("Forecasting of Tehran :");

            }
        });


        findViewById(R.id.btnMashad).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getWeatherMashad();
                city.setText("Forecasting of Mashad :");
            }
        });


        findViewById(R.id.btnIsfahan).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getWeatherIsfahan();
                city.setText("Forecasting of Isfahan :");
            }
        });


        findViewById(R.id.btnShiraz).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getWeatherShiraz();
                city.setText("Forecasting of Shiraz :");
            }
        });


        findViewById(R.id.btnTabriz).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getWeatherTabriz();
                city.setText("Forecasting of Tabriz :");
            }
        });


        findViewById(R.id.btnRasht).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getWeatherRasht();
                city.setText("Forecasting of Rasht :");
            }
        });


        findViewById(R.id.btnKerman).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getWeatherKerman();
                city.setText("Forecasting of Kerman :");
            }
        });


        findViewById(R.id.btnYazd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getWeatherYazd();
                city.setText("Forecasting of Yazd :");
            }
        });


        findViewById(R.id.btnAhvaz).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getWeatherAhvaz();
                city.setText("Forecasting of Ahvaz :");
            }
        });


    }

    void getWeatherTehran() {
        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    URL obj = new URL("http://api.openweathermap.org/data/2.5/weather?q=Tehran&units=metric&cnt=1&APPID=9706a776ca50587d08ab7e82b3924606");
                    HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                    con.setRequestMethod("GET");
                    con.setRequestProperty("User-Agent", "Mozilla/5.0");
                    int responseCode = con.getResponseCode();
                    if (responseCode == HttpURLConnection.HTTP_OK) {
                        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                        String inputLine;
                        StringBuffer response = new StringBuffer();
                        while ((inputLine = in.readLine()) != null) {
                            response.append(inputLine);
                        }

                        parseServerResponse1(response.toString());

                    }
                } catch (Exception e) {
                    Toast.makeText(WeatherActivity.this, "Error in connecting to website", Toast.LENGTH_SHORT).show();

                }

            }
        }).start();


    }


//    void parseByGson(String response){
//        Gson gson = new Gson();
//
//        WeatherModel weatherModel = gson.fromJson(response , WeatherModel.class);
//
//        tempInt.setText(weatherModel.getMain().getTemp().intValue());
//        pressureInt.setText(weatherModel.getMain().getPressure());
//        humidityInt.setText(weatherModel.getMain().getHumidity());
//        windsppedInt.setText(weatherModel.getWind().getSpeed().intValue());
//        weatherInt.setText(weatherModel.getWeather().get(0).getDescription());


//    }

    void parseServerResponse1(String response) {

        Log.d("WebService_Debug", response);

        try {
            JSONObject allObj = new JSONObject(response);
            String mainStr = allObj.getString("main");
            JSONObject mainObj = new JSONObject(mainStr);

            final String Temp = mainObj.getString("temp");
            final String Pressure = mainObj.getString("pressure");
            final String Humidity = mainObj.getString("humidity");

            String windStr = allObj.getString("wind");
            JSONObject windObj = new JSONObject(windStr);
            final String WindSpeed = windObj.getString("speed");

//            String weatherStr = allObj.getString("weather");
//            JSONObject weatherObj = new JSONObject(weatherStr);
//            String zeroStr = weatherObj.getString("0");
//            JSONArray zeroArray = new JSONArray(zeroStr);
//            final Int Weather = zeroArray.get("description");


            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    tempInt.setText(Temp);
                    pressureInt.setText(Pressure);
                    humidityInt.setText(Humidity);
                    windsppedInt.setText(WindSpeed);
//                      weatherInt.setText(Weather);

                }

            });


        } catch (JSONException e)

        {
            e.printStackTrace();
        }


    }

    void getWeatherMashad() {
        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    URL obj = new URL("http://api.openweathermap.org/data/2.5/weather?q=Mashhad&units=metric&cnt=1&APPID=9706a776ca50587d08ab7e82b3924606");
                    HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                    con.setRequestMethod("GET");
                    con.setRequestProperty("User-Agent", "Mozilla/5.0");
                    int responseCode = con.getResponseCode();
                    if (responseCode == HttpURLConnection.HTTP_OK) {
                        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                        String inputLine;
                        StringBuffer response = new StringBuffer();
                        while ((inputLine = in.readLine()) != null) {
                            response.append(inputLine);
                        }

                        parseServerResponse2(response.toString());

                    }
                } catch (Exception e) {
                    Toast.makeText(WeatherActivity.this, "Error in connecting to website", Toast.LENGTH_SHORT).show();

                }

            }
        }).start();


    }

    void parseServerResponse2(String response) {

        Log.d("WebService_Debug", response);

        try {
            JSONObject allObj = new JSONObject(response);
            String mainStr = allObj.getString("main");
            JSONObject mainObj = new JSONObject(mainStr);

            final String Temp = mainObj.getString("temp");
            final String Pressure = mainObj.getString("pressure");
            final String Humidity = mainObj.getString("humidity");

            String windStr = allObj.getString("wind");
            JSONObject windObj = new JSONObject(windStr);
            final String WindSpeed = windObj.getString("speed");


            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    tempInt.setText(Temp);
                    pressureInt.setText(Pressure);
                    humidityInt.setText(Humidity);
                    windsppedInt.setText(WindSpeed);

                }

            });


        } catch (JSONException e)

        {
            e.printStackTrace();
        }


    }

    void getWeatherIsfahan() {
        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    URL obj = new URL("http://api.openweathermap.org/data/2.5/weather?q=Esfahan&units=metric&cnt=1&APPID=9706a776ca50587d08ab7e82b3924606");
                    HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                    con.setRequestMethod("GET");
                    con.setRequestProperty("User-Agent", "Mozilla/5.0");
                    int responseCode = con.getResponseCode();
                    if (responseCode == HttpURLConnection.HTTP_OK) {
                        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                        String inputLine;
                        StringBuffer response = new StringBuffer();
                        while ((inputLine = in.readLine()) != null) {
                            response.append(inputLine);
                        }

                        parseServerResponse3(response.toString());

                    }
                } catch (Exception e) {
                    Toast.makeText(WeatherActivity.this, "Error in connecting to website", Toast.LENGTH_SHORT).show();

                }

            }
        }).start();


    }

    void parseServerResponse3(String response) {

        Log.d("WebService_Debug", response);

        try {
            JSONObject allObj = new JSONObject(response);
            String mainStr = allObj.getString("main");
            JSONObject mainObj = new JSONObject(mainStr);

            final String Temp = mainObj.getString("temp");
            final String Pressure = mainObj.getString("pressure");
            final String Humidity = mainObj.getString("humidity");

            String windStr = allObj.getString("wind");
            JSONObject windObj = new JSONObject(windStr);
            final String WindSpeed = windObj.getString("speed");


            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    tempInt.setText(Temp);
                    pressureInt.setText(Pressure);
                    humidityInt.setText(Humidity);
                    windsppedInt.setText(WindSpeed);

                }

            });


        } catch (JSONException e)

        {
            e.printStackTrace();
        }


    }

    void getWeatherShiraz() {
        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    URL obj = new URL("http://api.openweathermap.org/data/2.5/weather?q=Shiraz&units=metric&cnt=1&APPID=9706a776ca50587d08ab7e82b3924606");
                    HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                    con.setRequestMethod("GET");
                    con.setRequestProperty("User-Agent", "Mozilla/5.0");
                    int responseCode = con.getResponseCode();
                    if (responseCode == HttpURLConnection.HTTP_OK) {
                        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                        String inputLine;
                        StringBuffer response = new StringBuffer();
                        while ((inputLine = in.readLine()) != null) {
                            response.append(inputLine);
                        }

                        parseServerResponse4(response.toString());

                    }
                } catch (Exception e) {
                    Toast.makeText(WeatherActivity.this, "Error in connecting to website", Toast.LENGTH_SHORT).show();

                }

            }
        }).start();


    }

    void parseServerResponse4(String response) {

        Log.d("WebService_Debug", response);

        try {
            JSONObject allObj = new JSONObject(response);
            String mainStr = allObj.getString("main");
            JSONObject mainObj = new JSONObject(mainStr);

            final String Temp = mainObj.getString("temp");
            final String Pressure = mainObj.getString("pressure");
            final String Humidity = mainObj.getString("humidity");

            String windStr = allObj.getString("wind");
            JSONObject windObj = new JSONObject(windStr);
            final String WindSpeed = windObj.getString("speed");


            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    tempInt.setText(Temp);
                    pressureInt.setText(Pressure);
                    humidityInt.setText(Humidity);
                    windsppedInt.setText(WindSpeed);

                }

            });


        } catch (JSONException e)

        {
            e.printStackTrace();
        }


    }

    void getWeatherTabriz() {
        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    URL obj = new URL("http://api.openweathermap.org/data/2.5/weather?q=Tabriz&units=metric&cnt=1&APPID=9706a776ca50587d08ab7e82b3924606");
                    HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                    con.setRequestMethod("GET");
                    con.setRequestProperty("User-Agent", "Mozilla/5.0");
                    int responseCode = con.getResponseCode();
                    if (responseCode == HttpURLConnection.HTTP_OK) {
                        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                        String inputLine;
                        StringBuffer response = new StringBuffer();
                        while ((inputLine = in.readLine()) != null) {
                            response.append(inputLine);
                        }

                        parseServerResponse5(response.toString());

                    }
                } catch (Exception e) {
                    Toast.makeText(WeatherActivity.this, "Error in connecting to website", Toast.LENGTH_SHORT).show();

                }

            }
        }).start();


    }

    void parseServerResponse5(String response) {

        Log.d("WebService_Debug", response);

        try {
            JSONObject allObj = new JSONObject(response);
            String mainStr = allObj.getString("main");
            JSONObject mainObj = new JSONObject(mainStr);

            final String Temp = mainObj.getString("temp");
            final String Pressure = mainObj.getString("pressure");
            final String Humidity = mainObj.getString("humidity");

            String windStr = allObj.getString("wind");
            JSONObject windObj = new JSONObject(windStr);
            final String WindSpeed = windObj.getString("speed");


            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    tempInt.setText(Temp);
                    pressureInt.setText(Pressure);
                    humidityInt.setText(Humidity);
                    windsppedInt.setText(WindSpeed);

                }

            });


        } catch (JSONException e)

        {
            e.printStackTrace();
        }


    }

    void getWeatherRasht() {
        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    URL obj = new URL("http://api.openweathermap.org/data/2.5/weather?q=Rasht&units=metric&cnt=1&APPID=9706a776ca50587d08ab7e82b3924606");
                    HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                    con.setRequestMethod("GET");
                    con.setRequestProperty("User-Agent", "Mozilla/5.0");
                    int responseCode = con.getResponseCode();
                    if (responseCode == HttpURLConnection.HTTP_OK) {
                        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                        String inputLine;
                        StringBuffer response = new StringBuffer();
                        while ((inputLine = in.readLine()) != null) {
                            response.append(inputLine);
                        }

                        parseServerResponse6(response.toString());

                    }
                } catch (Exception e) {
                    Toast.makeText(WeatherActivity.this, "Error in connecting to website", Toast.LENGTH_SHORT).show();

                }

            }
        }).start();


    }

    void parseServerResponse6(String response) {

        Log.d("WebService_Debug", response);

        try {
            JSONObject allObj = new JSONObject(response);
            String mainStr = allObj.getString("main");
            JSONObject mainObj = new JSONObject(mainStr);

            final String Temp = mainObj.getString("temp");
            final String Pressure = mainObj.getString("pressure");
            final String Humidity = mainObj.getString("humidity");

            String windStr = allObj.getString("wind");
            JSONObject windObj = new JSONObject(windStr);
            final String WindSpeed = windObj.getString("speed");


            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    tempInt.setText(Temp);
                    pressureInt.setText(Pressure);
                    humidityInt.setText(Humidity);
                    windsppedInt.setText(WindSpeed);

                }

            });


        } catch (JSONException e)

        {
            e.printStackTrace();
        }


    }

    void getWeatherKerman() {
        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    URL obj = new URL("http://api.openweathermap.org/data/2.5/weather?q=Kerman&units=metric&cnt=1&APPID=9706a776ca50587d08ab7e82b3924606");
                    HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                    con.setRequestMethod("GET");
                    con.setRequestProperty("User-Agent", "Mozilla/5.0");
                    int responseCode = con.getResponseCode();
                    if (responseCode == HttpURLConnection.HTTP_OK) {
                        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                        String inputLine;
                        StringBuffer response = new StringBuffer();
                        while ((inputLine = in.readLine()) != null) {
                            response.append(inputLine);
                        }

                        parseServerResponse7(response.toString());

                    }
                } catch (Exception e) {
                    Toast.makeText(WeatherActivity.this, "Error in connecting to website", Toast.LENGTH_SHORT).show();

                }

            }
        }).start();


    }

    void parseServerResponse7(String response) {

        Log.d("WebService_Debug", response);

        try {
            JSONObject allObj = new JSONObject(response);
            String mainStr = allObj.getString("main");
            JSONObject mainObj = new JSONObject(mainStr);

            final String Temp = mainObj.getString("temp");
            final String Pressure = mainObj.getString("pressure");
            final String Humidity = mainObj.getString("humidity");

            String windStr = allObj.getString("wind");
            JSONObject windObj = new JSONObject(windStr);
            final String WindSpeed = windObj.getString("speed");


            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    tempInt.setText(Temp);
                    pressureInt.setText(Pressure);
                    humidityInt.setText(Humidity);
                    windsppedInt.setText(WindSpeed);

                }

            });


        } catch (JSONException e)

        {
            e.printStackTrace();
        }


    }

    void getWeatherYazd() {
        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    URL obj = new URL("http://api.openweathermap.org/data/2.5/weather?q=Yazd&units=metric&cnt=1&APPID=9706a776ca50587d08ab7e82b3924606");
                    HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                    con.setRequestMethod("GET");
                    con.setRequestProperty("User-Agent", "Mozilla/5.0");
                    int responseCode = con.getResponseCode();
                    if (responseCode == HttpURLConnection.HTTP_OK) {
                        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                        String inputLine;
                        StringBuffer response = new StringBuffer();
                        while ((inputLine = in.readLine()) != null) {
                            response.append(inputLine);
                        }

                        parseServerResponse8(response.toString());

                    }
                } catch (Exception e) {
                    Toast.makeText(WeatherActivity.this, "Error in connecting to website", Toast.LENGTH_SHORT).show();

                }

            }
        }).start();


    }

    void parseServerResponse8(String response) {

        Log.d("WebService_Debug", response);

        try {
            JSONObject allObj = new JSONObject(response);
            String mainStr = allObj.getString("main");
            JSONObject mainObj = new JSONObject(mainStr);

            final String Temp = mainObj.getString("temp");
            final String Pressure = mainObj.getString("pressure");
            final String Humidity = mainObj.getString("humidity");

            String windStr = allObj.getString("wind");
            JSONObject windObj = new JSONObject(windStr);
            final String WindSpeed = windObj.getString("speed");


            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    tempInt.setText(Temp);
                    pressureInt.setText(Pressure);
                    humidityInt.setText(Humidity);
                    windsppedInt.setText(WindSpeed);

                }

            });


        } catch (JSONException e)

        {
            e.printStackTrace();
        }


    }

    void getWeatherAhvaz() {
        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    URL obj = new URL("http://api.openweathermap.org/data/2.5/weather?q=Ahvaz&units=metric&cnt=1&APPID=9706a776ca50587d08ab7e82b3924606");
                    HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                    con.setRequestMethod("GET");
                    con.setRequestProperty("User-Agent", "Mozilla/5.0");
                    int responseCode = con.getResponseCode();
                    if (responseCode == HttpURLConnection.HTTP_OK) {
                        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                        String inputLine;
                        StringBuffer response = new StringBuffer();
                        while ((inputLine = in.readLine()) != null) {
                            response.append(inputLine);
                        }

                        parseServerResponse9(response.toString());

                    }
                } catch (Exception e) {
                    Toast.makeText(WeatherActivity.this, "Error in connecting to website", Toast.LENGTH_SHORT).show();

                }

            }
        }).start();


    }

    void parseServerResponse9(String response) {

        Log.d("WebService_Debug", response);

        try {
            JSONObject allObj = new JSONObject(response);
            String mainStr = allObj.getString("main");
            JSONObject mainObj = new JSONObject(mainStr);

            final String Temp = mainObj.getString("temp");
            final String Pressure = mainObj.getString("pressure");
            final String Humidity = mainObj.getString("humidity");

            String windStr = allObj.getString("wind");
            JSONObject windObj = new JSONObject(windStr);
            final String WindSpeed = windObj.getString("speed");


            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    tempInt.setText(Temp);
                    pressureInt.setText(Pressure);
                    humidityInt.setText(Humidity);
                    windsppedInt.setText(WindSpeed);

                }

            });


        } catch (JSONException e)

        {
            e.printStackTrace();
        }





    }
}





