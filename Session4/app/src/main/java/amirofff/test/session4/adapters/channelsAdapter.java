package amirofff.test.session4.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import amirofff.test.session4.R;
import amirofff.test.session4.models.ChannelModel;

/**
 * Created by macbook on 6/13/2017 AD.
 */

public class channelsAdapter extends BaseAdapter {
    Context mContext ;
    List<ChannelModel> models;
    Boolean isListview = true ;

    public channelsAdapter(Context mContext, List<ChannelModel> models, Boolean isListview) {
        this.mContext = mContext;
        this.models = models;
        this.isListview= isListview;
    }

    @Override
    public int getCount() {
        return models.size();
    }

    @Override
    public Object getItem(int position) {
        return models.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        View rowView = LayoutInflater.from(mContext).inflate(R.layout.channel_list_item, viewGroup, false);

        if (isListview==false)
            rowView = LayoutInflater.from(mContext).inflate(R.layout.channel_grid_item, viewGroup, false);

        ImageView channelLogo = (ImageView) rowView.findViewById(R.id.channelLogo);
        TextView channelName = (TextView) rowView.findViewById(R.id.channelName);

        Picasso.with(mContext).load(models.get(position).getChannelLogoURL()).into(channelLogo);
        channelName.setText(models.get(position).getChannelName());

        return rowView;
    }
}
