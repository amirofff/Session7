package amirofff.test.session4;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import amirofff.test.session4.models.StudentModel;

public class SugarDbActivity extends AppCompatActivity {


    EditText name;
    EditText family;
    EditText mobile;
    Button save;
    TextView showResults;
    DatabaseHandler handler;
    Button show;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sugar_db);


        name = (EditText) findViewById(R.id.name);
        family = (EditText) findViewById(R.id.family);
        mobile = (EditText) findViewById(R.id.mobile);
        save = (Button) findViewById(R.id.save);
        showResults =(TextView) findViewById(R.id.showResults);
        show = (Button) findViewById(R.id.show);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StudentModel model = new StudentModel();
                model.setName(name.getText().toString());
                model.setFamily(family.getText().toString());
                model.setMobile(mobile.getText().toString());
                model.save();

                name.setText("");
                family.setText("");
                mobile.setText("");
                Toast.makeText(SugarDbActivity.this, "data has been saved", Toast.LENGTH_SHORT).show();

            }
        });

        show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<StudentModel> students = StudentModel.listAll(StudentModel.class);
                String results = "";

                for (StudentModel student: students) {
                    results += student.getName() + " "
                            + student.getFamily() + " "
                            + student.getMobile() + "\n";
                }

                    showResults.setText(results);

            }
        });

    }
}
